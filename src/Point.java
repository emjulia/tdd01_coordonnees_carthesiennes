public class Point {
    private int x;
    private int y;

    public Point(int coordX, int coordY) {
        this.x = coordX;
        this.y = coordY;
    }

    public double calculerDistance(Point point) {
        double distance = Math.sqrt(Math.pow(point.x - this.x, 2) + Math.pow(point.y - this.y, 2));
        return (double)Math.round(distance * 10000) / 10000;
    }
}
