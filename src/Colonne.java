import java.util.Arrays;
import java.util.List;

public class Colonne {
    private List<Integer> pions =Arrays.asList(0, 0, 0, 0, 0, 0);

    public void ajouterPion() {
        ajouterPionSiLigneVide(3);
        ajouterPionSiLigneVide(2);
        ajouterPionSiLigneVide(1);
        ajouterPionSiLigneVide(0);
    }

    private void ajouterPionSiLigneVide(int ligne) {
        if (ligne == 0) {
            pions.set(ligne, 1);
        } else if (pions.get(ligne - 1) == 1) {
            pions.set(ligne, 1);
        }
    }

    public List<Integer> getPions() {
        return pions;
    }
}
