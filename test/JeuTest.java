import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test List:
 * - Pion R en C1 => C1 = (R, 0, 0, 0, 0, 0)
 * - Pion R en C2 => C2 = (R, 0, 0, 0, 0, 0)
 * - C1 = (R, 0, 0, 0, 0, 0) et Pion R en C1 => C1 = (R, R, 0, 0, 0, 0)
 * - Pion J en C1 => C1 = (J, 0, 0, 0, 0, 0)
 * - Pion J en C2 => C2 = (J, 0, 0, 0, 0, 0)
 * - C1 = (R, 0, 0, 0, 0, 0) et Pion R en C1 => C1 = (R, J, 0, 0, 0, 0)
 * - C1 = (R, R, J, J, R, R) et Pion R en C1 => ErreurColonnePleine
 * - condition de victoire ligne horizontale
 * - condition de victoire ligne verticale
 * - condition de victoire ligne diagonale
 */
class JeuTest {

    @Test
    public void unPionPremiereColonneVide() {
        // GIVEN
        Jeu jeu = new Jeu();
        Colonne colonne = jeu.getColonne(0);
        // WHEN
        colonne.ajouterPion();
        // THEN
        assertEquals(Arrays.asList(1, 0, 0, 0, 0, 0), jeu.getColonne(0).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(1).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(2).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(3).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(4).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(5).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(6).getPions());
    }

    @Test
    public void unPionPremiereColonneAvecUnPion() {
        // GIVEN
        Jeu jeu = new Jeu();
        Colonne colonne = jeu.getColonne(0);
        colonne.ajouterPion();
        // WHEN
        colonne.ajouterPion();
        // THEN
        assertEquals(Arrays.asList(1, 1, 0, 0, 0, 0), jeu.getColonne(0).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(1).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(2).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(3).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(4).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(5).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(6).getPions());
    }

    @Test
    public void unPionPremiereColonneAvecDeuxPions() {
        // GIVEN
        Jeu jeu = new Jeu();
        Colonne colonne = jeu.getColonne(0);
        colonne.ajouterPion();
        colonne.ajouterPion();
        // WHEN
        colonne.ajouterPion();
        // THEN
        assertEquals(Arrays.asList(1, 1, 1, 0, 0, 0), jeu.getColonne(0).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(1).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(2).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(3).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(4).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(5).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(6).getPions());
    }

    @Test
    public void unPionPremiereColonneAvecTroisPions() {
        // GIVEN
        Jeu jeu = new Jeu();
        Colonne colonne = jeu.getColonne(0);
        colonne.ajouterPion();
        colonne.ajouterPion();
        colonne.ajouterPion();
        // WHEN
        colonne.ajouterPion();
        // THEN
        assertEquals(Arrays.asList(1, 1, 1, 1, 0, 0), jeu.getColonne(0).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(1).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(2).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(3).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(4).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(5).getPions());
        assertEquals(Arrays.asList(0, 0, 0, 0, 0, 0), jeu.getColonne(6).getPions());
    }
}