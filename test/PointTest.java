import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {

    Point pointOrigine;
    Point pointPositif;
    Point pointNegatif;

    @BeforeEach
    public void setup() {
        pointOrigine = new Point(0,0);
        pointPositif = new Point(1, 1);
        pointNegatif = new Point(-1, -1);
    }

    /*
    Quelles actions on teste : Calcul de distance
    - coordonnées négatives

    - [1,1] - [2,2] : d = 1.4142
    - [1,1] - [0,0] : d = 1.4142
     */

    // NOTE : on garde pas cette méthode car la création est un prérequis du calcul de distance
//    @Test
//    public void creerUnPoint() {
//        // GIVEN
//        int coordX = 5;
//        int coordY = 5;
//        // WHEN
//        Point point = new Point(coordX, coordY);
//        // THEN
//        assertNotNull(point);
//        assertEquals(point.getX(), coordX);
//        assertEquals(point.getY(), coordY);
//    }

    @Test
    public void points_identiques() {
        // GIVEN
        Point point = new Point(0, 0);
        // WHEN
        double distance = pointOrigine.calculerDistance(point);
        // THEN
        assertEquals(0, distance);
    }

    @Test
    public void abscisse_plus_un() {
        // GIVEN
        Point point = new Point(1, 0);
        // WHEN
        double distance = pointOrigine.calculerDistance(point);
        // THEN
        assertEquals(1, distance);
    }

    @Test
    public void abscisse_positive_plus_un() {
        // GIVEN
        Point point = new Point(2, 1);
        // WHEN
        double distance = pointPositif.calculerDistance(point);
        // THEN
        assertEquals(1, distance);
    }

    @Test
    public void abscisse_moins_un() {
        // GIVEN
        Point point = new Point(-1, 0);
        // WHEN
        double distance = pointOrigine.calculerDistance(point);
        // THEN
        assertEquals(1, distance);
    }

    @Test
    public void abscisse_negative_moins_un() {
        // GIVEN
        Point point = new Point(-3, -1);
        // WHEN
        double distance = pointNegatif.calculerDistance(point);
        // THEN
        assertEquals(2, distance);
    }

    @Test
    public void ordonnee_plus_un() {
        // GIVEN
        Point point = new Point(0, 1);
        // WHEN
        double distance = pointOrigine.calculerDistance(point);
        // THEN
        assertEquals(1, distance);
    }

    @Test
    public void ordonnee_moins_un() {
        // GIVEN
        Point point = new Point(0, -1);
        // WHEN
        double distance = pointOrigine.calculerDistance(point);
        // THEN
        assertEquals(1, distance);
    }
    @Test
    public void diagonale() {
        // GIVEN
        Point point = new Point(2, 2);
        // WHEN
        double distance = pointOrigine.calculerDistance(point);
        // THEN
        assertEquals(2.8284, distance);
    }
}